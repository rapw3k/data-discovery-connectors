# Ensure to import only the neccesary library for connect to the storage solution selected
import subprocess
try:
    import psycopg2
except ImportError:
    subprocess.run(["pip", "install", "psycopg2-binary"])
    import psycopg2

###
# Connect to postgreSQL based on config_conn params and return metadata_obj
###
def db_conn(config_conn):    
    try:
        print("-> Trying to connect to PostgreSQL...")
        connection = psycopg2.connect(
            host=config_conn["db_host"],
            port=config_conn["db_port"],
            dbname=config_conn["db_name"],
            user=config_conn["db_user"],
            password=config_conn["db_pass"]            
        )
        cursor_obj = connection.cursor()
        cursor_obj.execute("SELECT version();")
        print("-> You are connected to:", cursor_obj.fetchone())
    except (Exception, psycopg2.Error) as error:
        print("-> Error while connecting to PostgreSQL:", error)
    finally:
        metadata_obj = []
        if connection:
            try:                
                # Get a list of all schemas in the database (excluding system schemas)
                cursor_obj.execute("SELECT schema_name FROM information_schema.schemata WHERE schema_name NOT IN ('pg_catalog','information_schema')")
                schemas = cursor_obj.fetchall()
                
                # TODO: Refer to issue #3 for more info
                for schema in schemas:
                    schema_name = schema[0]
                    
                    # Get a list of all tables in the schema
                    cursor_obj.execute(f"SELECT table_name FROM information_schema.tables WHERE table_schema = '{schema_name}'")
                    tables = cursor_obj.fetchall()
                    
                    for table in tables:
                        table_name = table[0]
                        
                        # Get a list of all columns in the table
                        cursor_obj.execute(f"SELECT column_name, data_type FROM information_schema.columns WHERE table_schema = '{schema_name}' AND table_name = '{table_name}'")
                        columns = cursor_obj.fetchall()
                       
                        # TODO: Refer to issue #4 for more info
                        # Add metadata info to the object
                        table_info = {
                            "schema": schema_name,
                            "table": table_name,
                            "columns": [{"column": column[0], "data_type": column[1]} for column in columns]
                        }
                        # TODO: Refer to issue #5 for more info
                        metadata_obj.append(table_info)
                print("-> Metadata info of PostgreSQL collected.")
            except psycopg2.Error as e:
                print(f"-> Error: {e}")
            finally:
                cursor_obj.close()
                connection.close()
        return metadata_obj