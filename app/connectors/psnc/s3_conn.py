# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    import boto3
except ImportError:
    subprocess.run(["pip", "install", "boto3"])
    import boto3


def db_conn(config_conn):
    try:
        print("-> Trying to connect to S3 server...")
        s3 = boto3.client(
            's3',
            aws_access_key_id=config_conn["aws_access_key_id"],
            aws_secret_access_key=config_conn["aws_secret_access_key"],
            endpoint_url=config_conn["url"]
        )

        # Initialize buckets list
        buckets = []

        # List all buckets in the S3 account
        buckets_response = s3.list_buckets()
        if "Buckets" in buckets_response:
            buckets = [bucket["Name"] for bucket in buckets_response["Buckets"]]
            print("-> You are connected to S3 server")

    except Exception as error:
        print("-> Error while connecting to S3 server:", error)
        return []

    metadata_obj = []
    try:
        for bucket_name in buckets:
            # Get a list of all objects in the bucket
            objects = s3.list_objects_v2(Bucket=bucket_name)["Contents"]

            # Add metadata info to the object
            bucket_info = {
                "bucket": bucket_name,
                "objects": [{"object": obj["Key"]} for obj in objects]
            }
            metadata_obj.append(bucket_info)
        print("-> Metadata info of S3 server collected.")
    except Exception as e:
        print(f"-> Error: {e}")
    finally:
        return metadata_obj
