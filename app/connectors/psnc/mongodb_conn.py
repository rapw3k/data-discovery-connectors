# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    from pymongo import MongoClient
except ImportError:
    subprocess.run(["pip", "install", "pymongo"])
    from pymongo import MongoClient


###
# Connect to MongoDB based on config_conn params and return metadata_obj
###
def db_conn(config_conn):
    try:
        print("-> Trying to connect to MongoDB...")
        connection = MongoClient(
            host=config_conn["db_host"],
            port=config_conn["db_port"],
            username=config_conn["db_user"],
            password=config_conn["db_pass"],
            authSource=config_conn["db_name"],
            authMechanism="SCRAM-SHA-256"  # Use SCRAM-SHA-256 authentication
        )

        # Access the specified database
        db = connection[config_conn["db_name"]]

        # List all collections in the database
        collections = db.list_collection_names()
        print("-> You are connected to:", config_conn["db_name"])

    except Exception as error:
        print("-> Error while connecting to MongoDB:", error)
    finally:
        metadata_obj = []
        if connection:
            try:
                for collection_name in collections:
                    # Get a list of all fields in the collection
                    collection = db[collection_name]
                    fields = collection.find_one()

                    # Add metadata info to the object
                    collection_info = {
                        "collection": collection_name,
                        "fields": [{"field": field, "data_type": type(fields[field]).__name__} for field in fields]
                    }
                    metadata_obj.append(collection_info)
                print("-> Metadata info of MongoDB collected.")
            except Exception as e:
                print(f"-> Error: {e}")
            finally:
                connection.close()
        return metadata_obj

