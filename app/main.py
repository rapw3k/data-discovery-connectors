import os
import json

# TODO: Refer to issue #2 for more info

try:
    # Get config. data
    with open('config_conn.json', 'r') as file:
        config_conn = json.load(file)
    db_type = config_conn['db_type']
    match db_type:
        case "elasticsearch":
            from connectors import elastic_conn
            result = elastic_conn.db_conn(config_conn)
        case "postgresql":
            from connectors import postgre_conn
            result = postgre_conn.db_conn(config_conn)            
        case _:
            result = "-> Error: No db_type value received"

    # Save the results
    if result:
        metadata_folder = "metadata"
        metadata_object = db_type+"_metadata_info.json"
        # Ensure that folder exists
        if not os.path.exists(metadata_folder):
            os.makedirs(metadata_folder)
        result_file_path = os.path.join(metadata_folder, metadata_object)
        with open(result_file_path, "w") as file:
            json.dump(result, file, indent=2)
        print("-> Metadata info saved to json file.")

    # TODO: Refer to issue #6 for more info

except Exception as error:
    print("-> Error in main process:", error)