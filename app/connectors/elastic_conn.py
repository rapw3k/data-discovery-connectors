# Ensure to import only the neccesary library for connect to the storage solution selected
import subprocess
try:
    from elasticsearch import Elasticsearch
except ImportError:
    subprocess.run(["pip", "install", "elasticsearch==7.15.2"])    
    from elasticsearch import Elasticsearch

###
# Connect to ElasticSearch based on config_conn params and return metadata_obj
###
def db_conn(config_conn):    
    try:
        print("-> Trying to connect to Elasticsearch...")
        es = Elasticsearch(
            hosts=[config_conn["db_host"]],
            port=config_conn["db_port"],
            http_auth=(config_conn["db_user"], config_conn["db_pass"])
        )        
        print("-> Connected to Elasticsearch:", es.info())
    except Exception as error:
        print("-> Error while connecting to Elasticsearch:", error)
    
    metadata_obj = []
    try:
        # Get a list of all indices in the cluster
        all_indexes = list(es.indices.get_alias().keys())

        for index_name in all_indexes:
            # Get the mapping for the current index
            index_mapping = es.indices.get_mapping(index=index_name)
            
            # Extract the field names and data types
            field_data_types = {}
            for index, index_mapping in index_mapping.items():
                for field_name, field_info in index_mapping['mappings']['properties'].items():
                    field_data_types[field_name] = field_info['type']

            # TODO: Refer to issue #4 for more info
            # Add metadata info to the object
            index_info = {
                "index_name": index_name,
                "fields": [{"field": field, "data_type": data_type} for field, data_type in field_data_types.items()]
            }
            # TODO: Refer to issue #5 for more info
            metadata_obj.append(index_info)
        print("-> Metadata info of Elasticsearch collected.")

    except Exception as e:
        print(f"-> Error: {e}")
    finally:
        return metadata_obj