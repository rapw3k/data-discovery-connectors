# Ensure to import only the necessary library for connecting to the storage solution selected
import subprocess
try:
    from SPARQLWrapper import SPARQLWrapper, JSON
except ImportError:
    subprocess.run(["pip", "install", "sparqlwrapper"])
    from SPARQLWrapper import SPARQLWrapper, JSON


###
# Connect to Virtuoso based on config_conn params and return metadata_obj
###
def db_conn(config_conn):
    try:
        print("-> Trying to connect to Virtuoso...")

        sparql = SPARQLWrapper(config_conn["endpoint"])
        sparql.setReturnFormat(JSON)

        # Example SPARQL query to retrieve metadata information
        sparql.setQuery("""
            select ?g where { graph ?g {?s ?p ?o} }
        """)

        results = sparql.query().convert()

        metadata_obj = []

        for result in results["results"]["bindings"]:
            metadata_info = {
                "graph": result["g"]["value"]
            }
            metadata_obj.append(metadata_info)

        print("-> Metadata info of Virtuoso collected.")

    except Exception as error:
        print("-> Error while connecting to Virtuoso:", error)
        metadata_obj = []

    return metadata_obj

